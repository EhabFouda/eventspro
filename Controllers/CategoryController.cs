﻿using Event.Application.Features.Category.Commands.CreateCategory;
using Event.Application.Features.Category.Commands.DeleteCategory;
using Event.Application.Features.Category.Commands.UpdateCategory;
using Event.Application.Features.Category.Queries.GetCategoryDetails;
using Event.Application.Features.Category.Queries.GetCategoryList;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Event.Web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly IMediator _mediator;

        public CategoryController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [HttpGet]
        public async Task<ActionResult<List<GetCategoryListViewModel>>> GetListCategories(Guid Id)
        {
            ViewBag.EventId = Id;
            return View(await _mediator.Send(new GetCategoryListQuery() { EventId = Id }));
        }

        [HttpGet]
        public async Task<ActionResult<GetCatgoryDetailsViewModel>> GetCategoryById(Guid Id)
        {
            return View(await _mediator.Send(new GetCategoryDetailsQuery() { CategoryId = Id }));
        }


        [HttpGet]
        public IActionResult CreateCategory(Guid EventId)
        {
            ViewBag.EventId = EventId;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateCategory([FromForm] CreateCategoryCommand createCategoryCommand)
        {
            Guid id = await _mediator.Send(createCategoryCommand);

            return RedirectToAction(nameof(GetListCategories), new { id = createCategoryCommand.EventId });
        }

   
        [HttpGet]
        public async Task<ActionResult<GetCatgoryDetailsViewModel>> UpdateCategory(Guid Id)
        {

            return View(await _mediator.Send(new GetCategoryDetailsQuery() { CategoryId = Id }));
        }

        [HttpPost]
        public async Task<ActionResult> UpdateCategory([FromForm] UpdateCategoryCommand updateCategoryCommand)
        {

            await _mediator.Send(updateCategoryCommand);

            return RedirectToAction(nameof(GetListCategories), new { Id = updateCategoryCommand.EventId });
        }


        [HttpPost]
        public async Task<ActionResult> DeleteCategory(Guid id)
        {
            await _mediator.Send(new DeleteCategoryCommand() { CategoryId = id });

            return Ok(new { key = 1 });
        }
    }
}
